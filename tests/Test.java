public class Test {
    public static void main(String[] args) {
        Kort kort1 = new Kort(null, Kort.Status.DOLT);
        Kort kort2 = kort1.copy();

        System.out.println("kort2.getStatus() = " + kort2.getStatus());
    }
}