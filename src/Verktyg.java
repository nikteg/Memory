import java.util.Random;

/**
 * Miscellaneous tools for Memory game
 * @author Fredrik Thune, Niklas Tegnander
 * @assignment 4
 * @group 9
 */
public class Verktyg {
    public static void slumpOrdning(Object[] m) {

        // Create new temporary array
        Object[] temp = new Object[m.length];

        // Copy and clean array
        for (int i = 0; i < m.length; i++) {
            temp[i] = m[i];
            m[i] = null;
        }

        // Set the array to random values from the temporary array
        for (int i = 0; i < m.length; i++) {
            int r = new Random().nextInt(m.length);

            while (m[r] != null) {
                r = new Random().nextInt(m.length);
            }

            m[r] = temp[i];
        }
    }
}
