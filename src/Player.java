import javax.swing.*;
import java.awt.*;

/**
 * Player class for Memory game
 * @author Fredrik Thune, Niklas Tegnander
 * @assignment 4
 * @group 9
 */
public class Player extends JLabel implements Comparable {
    private int score;
    private String name;

    /**
     * Create a new player with name
     * @param name the player's name
     */
    public Player(String name) {
        this.name = name;
        setHorizontalAlignment(JLabel.CENTER);
        setFont(new Font("Times", Font.PLAIN, 20));
        setOpaque(true);
        updateText();
    }

    /**
     * Get player name
     * @return the player name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the player name
     * @param name the player name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the player score
     * @return the player score
     */
    public int getScore() {
        return score;
    }

    /**
     * Set the player score
     * @param score the player score
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Add score to player
     * @param score the score to add
     */
    public void addScore(int score) {
        this.score += score;
    }

    /**
     * Update the label text
     */
    public void updateText() {
        setText("<html><center><b>" + getName() + "</b><br><br>"
                + getScore() + "</center></html>");
    }

    /**
     * Compare player score with another
     * @param o other player
     * @return whether to move up or down in the list
     */
    @Override
    public int compareTo(Object o) {
        if (getScore() > ((Player)o).getScore()) {
            return -1;
        } else if (getScore() < ((Player)o).getScore()) {
            return 1;
        } else {
            return 0;
        }
    }
}
