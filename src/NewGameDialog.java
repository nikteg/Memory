import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * New game dialog for Memory game
 * @author Fredrik Thune, Niklas Tegnander
 * @assignment 4
 * @group 9
 */
public class NewGameDialog extends JDialog implements ChangeListener {
    private JSlider numPlayersSlider;
    private JSlider numRowsSlider;
    private JSlider numColsSlider;
    private JButton startButton;
    private int max;

    /**
     * Create new game dialog
     * @param parent parent frame
     */
    NewGameDialog(final Memory parent, int max) {
        super(parent, "New game", true);
        this.max = max;

        // Exit the game when closing the window if the main frame
        // has not yet been shown
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowDeactivated(WindowEvent e) {
                if (!parent.isShowing()) {
                    System.exit(0);
                }
            }
        });

        startButton = new JButton("Start");
        startButton.setActionCommand("start");
        startButton.addActionListener((ActionListener)parent);
        JButton exitButton = new JButton("Cancel");
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NewGameDialog.this.dispose();
            }
        });

        numPlayersSlider = new JSlider(1, 5, 2);
        numPlayersSlider.setSnapToTicks(true);
        numPlayersSlider.setPaintTicks(true);
        numPlayersSlider.setPaintLabels(true);
        numPlayersSlider.setMajorTickSpacing(1);
        numPlayersSlider.setPreferredSize(new Dimension(640, 48));

        numRowsSlider = new JSlider(1, getMax()/4, 4);
        numRowsSlider.setSnapToTicks(true);
        numRowsSlider.setPaintTicks(true);
        numRowsSlider.setPaintLabels(true);
        numRowsSlider.setMajorTickSpacing(1);
        numRowsSlider.addChangeListener(this);
        numRowsSlider.setPreferredSize(new Dimension(640, 48));

        numColsSlider = new JSlider(1, getMax()/4, 4);
        numColsSlider.setSnapToTicks(true);
        numColsSlider.setPaintTicks(true);
        numColsSlider.setPaintLabels(true);
        numColsSlider.setMajorTickSpacing(1);
        numColsSlider.addChangeListener(this);
        numColsSlider.setPreferredSize(new Dimension(640, 48));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(3, 1));

        labelPanel.add(new JLabel("Players:"));
        labelPanel.add(new JLabel("Rows:"));
        labelPanel.add(new JLabel("Columns:"));

        JPanel controllerPanel = new JPanel();
        controllerPanel.setLayout(new GridLayout(3, 1));

        controllerPanel.add(numPlayersSlider, BorderLayout.CENTER);
        controllerPanel.add(numRowsSlider, BorderLayout.CENTER);
        controllerPanel.add(numColsSlider, BorderLayout.CENTER);

        // Fix margin
        labelPanel.setBorder(BorderFactory.createEmptyBorder(8, 16, 8, 8));
        controllerPanel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 16));

        setLayout(new BorderLayout());
        add(labelPanel, BorderLayout.WEST);
        add(controllerPanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(startButton);
        buttonPanel.add(exitButton);

        add(buttonPanel, BorderLayout.SOUTH);

        pack();
        setResizable(false);
        setLocationRelativeTo(parent);
    }

    /**
     * Get max value
     * @return the max value
     */
    public int getMax() {
        return max;
    }

    /**
     * Get number of players from the slider
     * @return the number of players
     */
    public int getNumPlayers() {
        return numPlayersSlider.getValue();
    }

    /**
     * Get number of rows from the slider
     * @return the number of rows
     */
    public int getNumRows() {
        return numRowsSlider.getValue();
    }

    /**
     * Get number of columns from the slider
     * @return the number of columns
     */
    public int getNumCols() {
        return numColsSlider.getValue();
    }

    /**
     * Set max value
     * @param max the max value
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * Set the maximum value on the players slider
     * @param v the maximum value
     */
    public void setMaxNumPlayers(int v) {
        numPlayersSlider.setMaximum(v);
    }

    /**
     * Set the maximum value on the rows slider
     * @param v the maximum value
     */
    public void setMaxNumRows(int v) {
        numRowsSlider.setMaximum(v);
    }

    /**
     * Set the maximum value on the columns slider
     * @param v the maximum value
     */
    public void setMaxNumCols(int v) {
        numColsSlider.setMaximum(v);
    }

    /**
     * Handle state changed events
     * @param e the event
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == numRowsSlider) {

            // Change the columns slider
            setMaxNumCols(getMax() / getNumRows());
            numColsSlider.repaint();
        } else if (e.getSource() == numColsSlider) {

            // Change the rows slider
            setMaxNumRows(getMax() / getNumCols());
            numRowsSlider.repaint();
        }

        if (e.getSource() == numRowsSlider || e.getSource() == numColsSlider) {

            // Change the players slider
            setMaxNumPlayers(getNumRows());
            numPlayersSlider.repaint();

            // Disable the start button if an odd value is given
            if (getNumRows() * getNumCols() % 2 == 0) {
                startButton.setEnabled(true);
            } else {
                startButton.setEnabled(false);
            }
        }
    }
}
