import javax.swing.*;
import java.awt.*;

/**
 * Card class for Memory game
 * @author Fredrik Thune, Niklas Tegnander
 * @assignment 4
 * @group 9
 */
public class Kort extends JButton {
    private Icon customIcon;
    private Status status;

    /**
     * Create card with default status
     * @param icon the icon
     */
    public Kort(Icon icon) {
        this(icon, Status.SAKNAS);
    }

    /**
     * Create card with a icon and a status
     * @param icon the icon
     * @param status the status
     */
    public Kort(Icon icon, Status status) {
        setFocusPainted(false);
        setCustomIcon(icon);
        setStatus(status);
        setOpaque(true);
    }

    /**
     * Set the custom icon
     * @param icon the icon
     */
    private void setCustomIcon(Icon icon) {
        this.customIcon = icon;
    }

    /**
     * Get the custom icon
     * @return the icon
     */
    private Icon getCustomIcon() {
        return customIcon;
    }

    /**
     * Set the status
     * Set background and icon according to status
     * @param status new status
     */
    public void setStatus(Status status) {
        switch (status) {
            case DOLT:
            case SYNLIGT:
                setBorder(BorderFactory.createLineBorder(new Color(0, 0, 200), 2));
                setBackground(Color.blue);
                break;
            case SAKNAS:
                setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
                setBackground(Color.white);
                break;
        }

        if (status == Status.SYNLIGT) {
            setIcon(getCustomIcon());
        } else {
            setIcon(null);
        }

        this.status = status;
    }

    /**
     * Get the status
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Makes a copy of this card
     * @return the new card
     */
    public Kort copy() {
        Kort copy = new Kort(getIcon(), getStatus());
        copy.setCustomIcon(getCustomIcon());
        return copy;
    }

    /**
     * Check if two images are equal
     * @return if they are equal
     */
    public boolean sammaBild(Kort other) {
        return other.getIcon() == null && getIcon() == null || (getIcon().equals(other.getIcon()));
    }

    /**
     * The status of the card
     * Can be DOLT, SYNLIGT or SAKNAS
     */
    public enum Status {
        DOLT, SYNLIGT, SAKNAS
    }
}
