import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Memory game in Swing
 * @author Fredrik Thune, Niklas Tegnander
 * @assignment 4
 * @group 9
 */
public class Memory extends JFrame implements ActionListener {
    private NewGameDialog newGameDialog;
    private ArrayList<Player> players = new ArrayList<Player>();
    private Timer timer = new Timer(1500, this);
    private Player currentPlayer;
    private ImageIcon[] icons;

    private Kort[] k;
    private Kort firstCard;
    private Kort secondCard;

    private JPanel gridPanel = new JPanel();
    private JPanel playersPanel = new JPanel();

    private int numPlayers;
    private int numRows;
    private int numCols;

    public static void main(String[] args) {
        new Memory();
    }

    /**
     * Set the next player
     */
    private void nextPlayer() {

        // Set the first player as the current player if no current player is defined
        if (currentPlayer == null) {
            currentPlayer = players.get(0);
        } else {

            // Get next player
            int index = players.indexOf(currentPlayer);
            index = ++index % numPlayers;
            currentPlayer = players.get(index);
        }

        for (Player p : players) {
            p.setBackground(Color.gray);
        }

        currentPlayer.setBackground(Color.yellow);
        setTitle(currentPlayer.getName() + "'s turn"); // Set window title
    }

    public Memory() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Memory");

        icons = fetchIcons();

        JPanel buttonsPanel = new JPanel();

        // New game button
        JButton newGame = new JButton("Nytt");
        newGame.addActionListener(this);
        newGame.setActionCommand("new");

        // Exit game button
        JButton quitGame = new JButton("Avsluta");
        quitGame.addActionListener(this);
        quitGame.setActionCommand("quit");

        buttonsPanel.add(newGame);
        buttonsPanel.add(quitGame);

        setLayout(new BorderLayout());

        add(playersPanel, BorderLayout.WEST);
        add(gridPanel, BorderLayout.EAST);
        add(buttonsPanel, BorderLayout.SOUTH);

        newGameDialog = new NewGameDialog(this, icons.length * 2);
        showNewGameDialog();
    }

    /**
     * Fetch image icons from image directory
     */
    private ImageIcon[] fetchIcons() {
        ImageIcon[] icons = {};

        try {
            File imageDirectory = new File("bildmapp/");

            // Fetch image files only from the image directory
            File files[] = imageDirectory.listFiles(new FileFilter() {

                @Override
                public boolean accept(File pathname) {
                    return (pathname.isFile() && pathname.getName().matches("(?i).*\\.(png|jpg|gif)"));
                }
            });

            // Create new k with an ImageIcon containing the image
            icons = new ImageIcon[files.length];
            for (int i = 0; i < files.length; i++) {
                icons[i] = new ImageIcon(files[i].getPath());
            }
        } catch (NullPointerException e) {
            System.err.println("Image folder could not be found");
            System.exit(1);
        }

        return icons;
    }

    /**
     * Show the new game dialog
     */
    private void showNewGameDialog() {
        newGameDialog.setVisible(true);
        newGameDialog.setLocationRelativeTo(this);
    }

    /**
     * Start a new game
     */
    public void nyttSpel() {

        // Reset game
        timer.stop();
        firstCard = null;
        secondCard = null;
        currentPlayer = null;
        playersPanel.removeAll();
        gridPanel.removeAll();
        players.clear();

        // Create new players and add the to the panel
        playersPanel.setLayout(new GridLayout(numPlayers, 1));
        for (int i = 0; i < numPlayers; i++) {
            Player p = new Player("Player " + (i + 1));
            playersPanel.add(p);
            players.add(p);
        }

        // Set the next player
        nextPlayer();

        int num = numRows * numCols;
        gridPanel.setLayout(new GridLayout(numRows, numCols));

        if (num > (icons.length * 2)) {
            JOptionPane.showMessageDialog(null, "Maximum number of cards is "
                    + icons.length, "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
            return;
        }

        k = new Kort[num];

        /**
         * Vi valde att göra såhär istället för hur det beskrevs
         * på labbsidan, då det uppfyller samma sak i bara en loop.
         * Detta gör också att vi bara behöver sortera en gång.
         */

        // Create cards and copies from icons
        for (int i = 1; i < num; i+=2) {
            k[i-1] = new Kort(icons[((i - 1) / 2)]);
            k[i] = k[i-1].copy();
        }

        // Shuffle the cards
        Verktyg.slumpOrdning(k);

        // Add cards to panel
        for (Kort card : k) {
            card.setStatus(Kort.Status.DOLT);
            card.addActionListener(this);
            gridPanel.add(card);
        }

        // Set new dimensions
        playersPanel.setPreferredSize(new Dimension(128, 100 * numRows));
        gridPanel.setPreferredSize(new Dimension(100 * numCols, 100 * numRows));
        getContentPane().setPreferredSize(new Dimension(128 + 100 * numCols, 100 * numRows + 64));

        pack();
        setLocationRelativeTo(null);
    }

    /**
     * Get number of cards still active in the game
     * @return the number of living cards
     */
    private int getNumberOfLivingCards() {
        int num = 0;

        for (Kort card : k) {
            if (card.getStatus() != Kort.Status.SAKNAS) {
                num++;
            }
        }

        return num;
    }

    /**
     * Get the winner or if the game was a tie
     * @return the winner (null if the game was a tie)
     */
    private Player getWinner() {
        Collections.sort(players);
        Player winner = players.get(0);
        boolean tie = false;

        for (int i = 1; i < players.size(); i++) {
            if (players.get(i).getScore() == winner.getScore()) {
                tie = true;
            }
        }

        return (tie) ? null : winner;
    }

    /**
     * Handle actions events
     * @param e event
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand() != null && e.getActionCommand().equals("new")) {

            // Show new game dialog
            showNewGameDialog();
        } else if (e.getActionCommand() != null && e.getActionCommand().equals("start")) {

            // Get values from the new game dialog
            setVisible(true);
            setLocationRelativeTo(null);
            newGameDialog.setVisible(false);
            numPlayers = newGameDialog.getNumPlayers();
            numRows = newGameDialog.getNumRows();
            numCols = newGameDialog.getNumCols();

            // Start a new game
            nyttSpel();
        } else if (e.getActionCommand() != null && e.getActionCommand().equals("quit")) {

            // Exit game
            System.exit(0);
        } else if (e.getSource() instanceof Kort) {

            // Clicked card
            Kort card = ((Kort) e.getSource());

            // Do nothing if the card is already found or the timer is running
            // Also make sure the player cannot press the same card twice
            if (card.getStatus() == Kort.Status.SAKNAS || timer.isRunning()
                    || card.equals(firstCard)) {
                return;
            }

            // Set the clicked card's status
            card.setStatus(Kort.Status.SYNLIGT);

            if (firstCard == null) {
                firstCard = card;
            } else if (secondCard == null) {
                secondCard = card;
                timer.start();
            }
        } else if (e.getSource() == timer) {
            if (firstCard != null && secondCard != null) {

                // If the cards match
                if (firstCard.sammaBild(secondCard)) {
                    firstCard.setStatus(Kort.Status.SAKNAS);
                    secondCard.setStatus(Kort.Status.SAKNAS);

                    currentPlayer.addScore(1);
                    currentPlayer.updateText();

                    // If no cards are left
                    if (getNumberOfLivingCards() == 0) {
                        Player winner = getWinner();
                        String winnerStr =
                                (winner == null) ? "The game was a tie"
                                        : "The winner is " + winner.getName() + "!";

                        JOptionPane.showMessageDialog(this, "Game over!\n" + winnerStr);

                        showNewGameDialog();
                    }
                } else {

                    // Hide the cards if the cards did not match
                    firstCard.setStatus(Kort.Status.DOLT);
                    secondCard.setStatus(Kort.Status.DOLT);

                    // Set the next player
                    nextPlayer();
                }

                // Remove clicked cards
                firstCard = null;
                secondCard = null;
            }

            timer.stop();
        }
    }
}
